#lang racket
(require test-engine/racket-tests)
(require "../days/day17.rkt")

; 0 1 2
; 3 4 5
; 6 7 8

(check-expect (get-coords 5 3 2) '(2 1))
(check-expect (get-coords 8 3 2) '(2 2))
(check-expect (get-pos '(2 1) 3) 5)
(check-expect (get-pos '(2 2) 3) 8)

; 0 1
; 2 3
;
; 4 5
; 6 7
(check-expect (get-coords 3 2 3) '(1 1 0))
(check-expect (get-coords 6 2 3) '(0 1 1))
(check-expect (get-pos '(1 1 0) 2) 3)
(check-expect (get-pos '(0 1 1) 2) 6)

; pos is relative to next state, so 0 is left of our seed
(check-expect (get-next-state [λ(me _) (not me)] 0 #(#t) 1 1) #t)
(check-expect (get-next-state [λ(me _) (not me)] 1 #(#t) 1 1) #f);

(check-expect (rule-conway #t 1) #f)
(check-expect (rule-conway #t 2) #t)
(check-expect (rule-conway #t 3) #t)
(check-expect (rule-conway #t 4) #f)

(check-expect (rule-conway #f 2) #f)
(check-expect (rule-conway #f 3) #t)
(check-expect (rule-conway #f 4) #f)

(check-expect (get-next-state
               rule-conway
               (get-pos '(0 0 0) 3)
               #(#f #t #f
                 #f #f #t
                 #t #t #t)
               3 3) #f)

(define initial-state
  (let ([vec (make-vector (* 3 3 3))])
    (vector-copy! vec (* 3 3)
                  (parse-layer
                   '(".#."
                     "..#"
                     "###")))
    vec))

(check-expect (get-next-state
               rule-conway
               (get-pos '(0 0 0) 3)
               (parse-layer
                '(".#."
                  "..#"
                  "###"))
               3 3) #f)

(test)