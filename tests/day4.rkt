#lang racket
(require test-engine/racket-tests)
(require "../days/day4.rkt")

(define (passport-list)
  (string-split
  "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in" "\n"))

(define (parsed-passports)
  (reverse (parse-passports (passport-list))))
  

(check-expect
 (parse-values "abc:123 def:567 racket:great")
 (reverse '(("abc" "123") ("def" "567") ("racket" "great"))))
(check-expect
 (match/values (parse-passport (passport-list))
               [(hash rem) hash])
 #hash(("ecl" . "gry") ("pid" . "860033327") ("eyr" . "2020") ("hcl" . "#fffffd")
                       ("byr" . "1937") ("iyr" . "2017") ("cid" . "147") ("hgt" . "183cm")))
(check-expect (length (parse-passports (passport-list))) 4)
(check-expect (validate-passport (first (parsed-passports))) #t)
(check-expect (validate-passport (second (parsed-passports))) #f)
(check-expect (validate-passport (third (parsed-passports))) #t)
(check-expect (validate-passport (fourth (parsed-passports))) #f)

(test)