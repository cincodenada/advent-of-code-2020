#lang racket
(require "common.rkt")

(provide read-group)

(define (get-answers line)
  (list->set (string->list line)))

(define (merge-answers line set)
  (set-union set (get-answers line)))

(define (intersect-answers line set)
  (set-intersect set (get-answers line)))

(define (read-group lines merge-proc)
  (define (read-group lines set)
    (match lines
      ['() (values set empty)]
      [(cons "" rem) (values set rem)]
      [(cons line rem) (read-group rem (merge-proc line set))]))
  (read-group (rest lines) (get-answers (first lines))))

(define (read-groups lines merge-proc)
  (define (read-groups lines groups)
    (match/values
     (read-group lines merge-proc)
     [(group '()) (cons group groups)]
     [(group rem) (read-groups rem (cons group groups))]))
  (read-groups lines '()))

(define (solve part)
  (match part
    [1 (apply + (map set-count (read-groups (read-lines (input 6)) merge-answers)))]
    [2 (apply + (map set-count (read-groups (read-lines (input 6)) intersect-answers)))]
    ))